#[cfg(test)]
pub(crate) fn get_2_mut<T>(slice: &mut [T], a: usize, b: usize) -> (&mut T, &mut T) {
	assert_ne!(a, b);

	if a > b {
		let (b, a) = get_2_mut(slice, b, a);
		return (a, b)
	}

	let (slice, b) = slice.split_at_mut(b);
	(&mut slice[a], &mut b[0])
}

pub(crate) fn map_in_place<T, E>(
	vec: &mut Vec<T>,
	mut f: impl FnMut(T) -> Result<T, E>,
) -> Result<(), E> {
	// This could probably be done more efficiently with unsafe code. Just ptr copy out the elements to map them, on error or panic just truncate the Vec without running destructors (or swap-remove the offending element and forget the last element).

	if vec.is_empty() { return Ok(()) }

	let last = vec.len() - 1;
	for i in 0..vec.len() {
		let e = vec.swap_remove(i);
		vec.push(f(e)?);
		vec.swap(i, last);
	}
	Ok(())
}
