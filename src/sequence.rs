pub trait SequenceFactory {
	type Sequence: Clone + std::hash::Hash + Ord;
	type Delta: std::ops::Add<Output=Self::Delta> + Clone;

	fn base() -> Self::Sequence;
	fn acquire(&mut self, last: Self::Sequence) -> Self::Sequence;

	fn rewind(&self, current: Self::Sequence, amount: &Self::Delta) -> Self::Sequence;
}
