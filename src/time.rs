#[derive(Debug, Default)]
pub struct TimeSequenceFactory {
	_extensible: (),
}

impl crate::SequenceFactory for TimeSequenceFactory {
	type Sequence = std::time::SystemTime;
	type Delta = std::time::Duration;

	fn base() -> std::time::SystemTime {
		std::time::UNIX_EPOCH
	}

	fn acquire(&mut self, last: std::time::SystemTime) -> std::time::SystemTime {
		let now = std::time::SystemTime::now();

		if now <= last {
			return last + std::time::Duration::from_nanos(1)
		}

		now
	}

	fn rewind(&self, current: std::time::SystemTime, amount: &std::time::Duration) -> std::time::SystemTime {
		current - *amount
	}
}
